# Some nifty pieces of python code that I re-use ofter

And might as well turn into a `pip` package someday. But not today

## Square-grid coordinates manipulation

Useful in application working with 2D discrete grid coordinates, such as games

Some useful definitions first:

~~~ {.python}
Point = namedtuple("Point", ["x", "y"])


def distance(a: Point, b: Point):
    return math.sqrt(((a.x - b.x) ** 2) + ((a.y - b.y) ** 2))

def manhattan(a: Point, b: Point):
    return abs(a.x - b.x) + abs(a.y - b.y)
~~~

---

Generate the rectangular area *span*ning around a *center*.
The "newlines" thing is a bit hackish, but it works fine in practice.

~~~ {.python}
def surroundings(center: Point, span: Point, newlines=False):
    extra = Point(span.x % 2, span.y % 2)
    for y in range(center.y - (span.y // 2), center.y + (span.y // 2) + extra.y, 1):
        for x in range(center.x - (span.x // 2), center.x + (span.x // 2) + extra.x, 1):
            yield Point(x, y)
        if newlines:
            yield "\n"
~~~

---

Generate all the cells "spiraling" away from a *center*; stop when the spiral has
filled a rectangular *maxsize* area around the center.  
This snippet is basically equivalent to the previous one, but it generates the
cells in a different order.

~~~ {.python}
def spiral(center: Point, maxsize: Point=(8, 8)):
    X, Y = maxsize
    x = y = 0
    dx = 0
    dy = -1
    for i in range(max(X, Y)**2):
        if (-X/2 < x <= X/2) and (-Y/2 < y <= Y/2):
            yield Point(x + center.x, y + center.y)
        if x == y or (x < 0 and x == -y) or (x > 0 and x == 1-y):
            dx, dy = -dy, dx
        x, y = x+dx, y+dy
~~~