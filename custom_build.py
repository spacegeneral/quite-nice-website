import pypandoc
import os
import argparse
import shutil
import imghdr
from subprocess import call, check_output
import http.server
import socketserver
import datetime


sources_prefix = "./sources"
prefab_prefix = "./prefab"
base_url = "/quite-nice-website/"
is_testing = False


prefab = {}
for fname in os.listdir(prefab_prefix):
    resname = fname[:-len(".html")]
    with open("%s/%s" % (prefab_prefix, fname), "r") as fp:
        prefab[resname] = fp.read()


class Page:
    def __init__(self, title, sourceurl, url, path, core, is_folder_index=False):
        self.title = title
        self.sourceurl = sourceurl
        self.url = url
        self.path = path
        self.core = core
        self.is_folder_index = is_folder_index


pages = {}
link_translate = {}
img_translate = {}



def process_md_file(path, name):
    print("pre-processing page %s" % path)
    title = name[:-len(".md")]
    invariant_path = path[len(sources_prefix + "/"):]
    invariant_path_htmlified = invariant_path[:-len(".md")] + ".html"

    if invariant_path == "index.md":
        newpath = "./" + invariant_path_htmlified
        url = base_url + invariant_path_htmlified
    else:
        newpath = "./pages/" + invariant_path_htmlified
        url = base_url + "pages/" + invariant_path_htmlified

    if url.endswith("index.html"):
        url = url[:-len("index.html")]

    core = pypandoc.convert_file(path, "html")

    link_translate[invariant_path_htmlified] = url
    pages[title] = Page(title, invariant_path_htmlified, url, newpath, core)


def process_folder(path, name):
    print("pre-processing folder %s" % path)
    title = name
    invariant_path = path[len(sources_prefix + "/"):]

    newpath = "./pages/" + invariant_path + "/index.html"
    url = base_url + "pages/" + invariant_path + "/"
    pages[title] = Page(title, invariant_path, url, newpath, "", is_folder_index=True)


def postprocess_folders(pages):
    normalpages = list(filter(lambda p: not p.is_folder_index, pages))
    for folderpage in filter(lambda f: f.is_folder_index, pages):
        outgoing_pages = filter(lambda n: n.url.startswith(folderpage.url), normalpages)
        folderpage.core += "<h1>Pages published in <i>%s</i>:</h1>" % folderpage.title
        folderpage.core += "<ul>"
        for outpage in outgoing_pages:
            folderpage.core += "<li><a href=\"%s\">%s</a></li>" % (outpage.sourceurl, outpage.title)
        folderpage.core += "</ul>"


def process_image_file(path, name):
    print("processing image %s" % path)
    invariant_path = path[len(sources_prefix + "/"):]
    newpath = "./images/" + invariant_path
    url = base_url + "images/" + invariant_path

    os.makedirs(os.path.dirname(newpath), exist_ok=True)
    img_translate[invariant_path] = url

    imgtype = imghdr.what(path)
    if imgtype == "png":
        call("convert -interlace PNG \"%s\" \"%s\"" % (path, newpath),shell=True)
    elif imgtype == "jpeg":
        call("convert -interlace Plane \"%s\" \"%s\"" % (path, newpath),shell=True)
    else:
        shutil.copy(path, newpath)


def process_source_file(path, name):
    if name.endswith(".md"):
        process_md_file(path, name)
    elif imghdr.what(path) is not None:
        process_image_file(path, name)
    else:
        print("Cannot process unknown file %s" % path)


def make_topbar(pages, current_page):
    index = next(filter(lambda p: p.sourceurl == "index.html", pages))
    folderpages = list(filter(lambda f: f.is_folder_index, pages))
    landmark_pages = [index] + folderpages

    ret = "<div class=\"topbar\">"
    for i,landmark in enumerate(landmark_pages):
        is_current = current_page.url.startswith(landmark.url)
        if i == 0:
            is_current = (current_page == index)
        before, after = "", "|"
        if i >= len(landmark_pages)-1:
            after = ""
        if is_current: before, after = "[", "]"
        if before != "":
            ret += "<span class=\"landmark\">%s</span>" % before
        ret += "<span class=\"landmark\"><a href=\"%s\">%s</a></span>" % (landmark.url, landmark.title)
        if after != "":
            ret += "<span class=\"landmark\">%s</span>" % after
    ret += "</div>"

    return ret


def make_bottombar():
    dateformat = "%A, %d %B %Y %H:%M:%S %Z"
    dategen = datetime.datetime.now().strftime(dateformat)
    version = check_output("git describe --dirty --always --tags", shell=True).decode("utf-8")

    bottomtext = "Generated in %s by custom_build.py version %s%s" % (
        dategen,
        version,
        "(test build)" if is_testing else ""
    )

    return "<div class=\"bottombar\">%s</div>" % bottomtext


def fmt_prefetches(page_prefetch_links):
    ret = ""
    for link in page_prefetch_links:
        ret += "<link rel=\"preload\" href=\"%s\">" % link
    return ret


def fix_references(body):
    # Fix links
    for oldlink, newlink in link_translate.items():
        if body.find(oldlink) >= 0:
            body = body.replace(oldlink, newlink)
    # Fix images
    for oldsrc, newsrc in img_translate.items():
        body = body.replace(oldsrc, newsrc)

    return body


def find_references(body):
    page_prefetch_links = []
    for oldlink, newlink in link_translate.items():
        if body.find(oldlink) >= 0:
            page_prefetch_links.append(newlink)
    return page_prefetch_links


def write_pages():
    postprocess_folders(pages.values())

    for title, page in pages.items():
        print("writing page \"%s\"" % title)
        page_prefetch_links = find_references(page.core)
        # Make dir structure
        os.makedirs(os.path.dirname(page.path), exist_ok=True)
        # Write html file
        with open(page.path, "w") as fp:
            newpage = "<!DOCTYPE html><html><head>"
            newpage += prefab["head_header"]
            newpage += fmt_prefetches(page_prefetch_links)
            newpage += "<title>%s</title>" % page.title
            newpage += "</head><body>"
            newpage += prefab["body_header"]
            newpage += make_topbar(pages.values(), page)
            newpage += "<div id=\"core\">"
            newpage += page.core
            newpage += "</div>"
            # TODO: actual toc
            newpage += prefab["footer"]
            newpage += make_bottombar()
            newpage += "</body></html>"

            fixedpage = fix_references(newpage)
            fp.write(fixedpage)


def serve(cli):
    Handler = http.server.SimpleHTTPRequestHandler
    with socketserver.TCPServer(("", cli.port), Handler) as httpd:
        print("serving at port", cli.port)
        httpd.serve_forever()


def parsecli():
    parser = argparse.ArgumentParser(description="Automatic markdown to html static site builder")
    parser.add_argument('-t', '--test', help='build for testing locally from file', action="store_true")
    parser.add_argument('-s', '--serve', help='start basic web server for local testing', action="store_true")
    parser.add_argument('-p', '--port', help='port to serve http content from (used with -s)',
                        type=int, default=8000)

    return parser.parse_args()

if __name__ == "__main__":
    cli = parsecli()

    if cli.test:
        is_testing = True
        base_url = "/"

    link_translate["style/custom.css"] =  base_url + "style/custom.css"
    link_translate["style/panam.css"] = base_url + "style/panam.css"
    link_translate["style/locutus.css"] = base_url + "style/locutus.css"
    link_translate["fonts/b1.ttf"] = base_url + "fonts/b1.ttf"
    link_translate["fonts/b2.ttf"] = base_url + "fonts/b2.ttf"
    link_translate["fonts/b3.ttf"] = base_url + "fonts/b3.ttf"

    print("scanning source files")
    for root, dirs, files in os.walk(sources_prefix):
        for file in files:
            process_source_file(os.path.join(root, file), file)
        for dir in dirs:
            process_folder(os.path.join(root, dir), dir)
    write_pages()

    if cli.serve:
        serve(cli)
